package com.example.demo;

import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/response")
public class TestMultipart {

	@GetMapping
	public void get() throws URISyntaxException {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(getCOnvert());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
		MultiValueMap formData = restTemplate.getForObject("http://localhost:8080/test", MultiValueMap.class);

//		ResponseEntity<HttpServletResponse> response = restTemplate.exchange("http://localhost:8080/test", HttpMethod.GET, null, HttpServletResponse.class);

		/*
		 * RequestEntity request = RequestEntity .get(new
		 * URI("http://localhost:8080/test"))
		 * .accept(MediaType.MULTIPART_FORM_DATA).build();
		 */
//		HttpEntity<HttpEntity> exchange = restTemplate.exchange("http://localhost:8080/test", HttpMethod.GET, null, HttpEntity.class);
		System.out.println("test");
	}

	@PostMapping
	public void update() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
		restTemplate.getMessageConverters().add(getCOnvert());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.MULTIPART_FORM_DATA));
		headers.setAcceptCharset(Arrays.asList(Charset.defaultCharset()));

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<MultiValueMap> response = restTemplate.exchange("http://localhost:8080/test", HttpMethod.GET, entity,
				MultiValueMap.class);
	}

	private FormHttpMessageConverter getCOnvert() {
		FormHttpMessageConverter formConverter = new FormHttpMessageConverter() {
			@Override
			public boolean canRead(Class<?> clazz, MediaType mediaType) {
				if (clazz == MultiValueMap.class) {
					return true;
				}
				return super.canRead(clazz, mediaType);
			}
		};

		return formConverter;
	}
}
